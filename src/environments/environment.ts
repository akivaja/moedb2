// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCDd3ax4IBNv6OYerh8WiBy3GeQbn8ospA",
    authDomain: "moedb2.firebaseapp.com",
    databaseURL: "https://moedb2.firebaseio.com",
    projectId: "moedb2",
    storageBucket: "moedb2.appspot.com",
    messagingSenderId: "626534917037",
    appId: "1:626534917037:web:15e1a8bc62caab91a4f724"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
